
YAML.load(File.read("config/gce.yml")).with_indifferent_access[Rails.env].each do |k, v|
  Paperclip::Attachment.default_options[k.to_sym] = v
end