Raven.configure do |config|
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)

  config.dsn = 'http://8751fc29f61a42beb078354947483fcc:e798f5d9f0f54fd1b20975397f6bbc30@sentry.ingedata.net/8'
  config.environments = %w(development staging production)
end

