Rails.application.routes.draw do

  #authenticated :user do
    root :to => "home#index"
  #end

  #root :to => redirect("/users/sign_in")

  resources :quizzes, only: [] do
    get "question"
    post "answer"
    get "result"
  end

	post '/home', to: 'home#quiz_session', :defaults => { :rank => '1' }

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)


  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end