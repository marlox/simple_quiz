require 'test_helper'

class Admin::SettingsRouteTest < ActionDispatch::IntegrationTest
  test 'create' do
    assert_raises(ActionController::RoutingError) do
      post '/admin/settings'
    end
  end

  test 'destroy' do
    assert_raises(ActionController::RoutingError) do
      delete '/admin/settings/1'
    end
  end

  test 'edit' do
    assert_routing '/admin/settings/1/edit', controller: 'admin/settings', action: 'edit', id: '1'
  end

  test 'index' do
    assert_routing '/admin/settings', controller: 'admin/settings', action: 'index'
  end

  test 'new' do
    assert_routing '/admin/settings/new', controller: 'admin/settings', action: 'show', id: 'new'
  end

  test 'update' do
    assert_routing({ method: 'patch', path: '/admin/settings/1' }, { controller: 'admin/settings', action: 'update', id: '1' })
  end
end