require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest

  def setup
    #login_as_user role: :admin
    #login_as @user, scope: :user
    @quiz = Quiz.find.first
  end

########## APPLICANT SHOULD HAVE ACCESS IF NOT LOGGED IN #############

  test "Has access to Sign-In Page" do
    get new_user_session_path
    assert_response 200, flash[:alert]
  end

  test "Has access to Sign-up Page" do
    get new_user_registration_path
    assert_response 200, flash[:alert]
  end

  test "Has access to Admin Login Page" do
    get admin_root_path
    assert_not_equal 200, flash[:alert]
  end


########## APPLICANT SHOULD NOT HAVE ACCESS IF NOT LOGGED IN #############

  test "Has access to Home" do
    get root_path
    assert_response 200, flash[:alert]
  end

  test "Has access to Applicant Profile" do
    get edit_user_registration_path(@user)
    assert_response 200, flash[:alert]
  end

  test "Has access to Exam" do
    get quiz_question_path(@quiz, @quiz.id)
    assert_not_equal nil, flash[:alert]
  end

  test "Has access to Result Page" do
    get quiz_result_path(@quiz, @quiz.id)
    assert_not_equal nil, flash[:alert]
  end

  test "Has access to Admin Dashboard" do
    get admin_dashboard_path
    assert_not_equal nil, flash[:alert]
  end

end
