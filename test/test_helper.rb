ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActionController::TestCase
  include Devise::Test::ControllerHelpers
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  include ActiveJob::TestHelper
  fixtures :all

class ActionDispatch::IntegrationTest
  include Warden::Test::Helpers

  # def login_as_user role: :admin

  #   @user = create(:user) # User.create! email: "test@test.com", password: "password"
  #   @user.project_roles << Project::Role[:default_role]

  #   if role
  #     @user.security_roles << Security::Role[role]
  #   end

  #   @team = User::Team.create! name: "test_team"

  #   @team.users << @user
  #   @project.teams << @team

  #   login_as @user, scope: :user
  # end

end

  # Add more helper methods to be used by all tests here...

end
