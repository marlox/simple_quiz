# SIMPLE QUIZ

An application which enables a company or school to use paperless examination for their applicants or students.
 
 
## Main Features
 
### 1. Take an Exam
* Multiple Choice
* Questions
* Timer
* Time Elapsed Indicator
* Maximum Limit of Exam Taken
 
### 2. Manage Quiz
* Set of Quizzes (Exam)
* Quiz with Questions
* Questions with Multiple Choices
* Image Attachment
* Quiz Results
 
### 3. Access Rights
* User
* Admin
 
### 4. Others
* Automatically sends an email when Quiz has been finished
 
 
## Specifications
* Ruby Version: 2.3.1
* Rails Version: 5.0.0.1
* Database: PostgreSQL
