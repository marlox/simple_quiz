module HomeHelper

  def quiz_session_create qs_id, qs_time, exam_id
    session[:quiz_session_id] = qs_id
    session[:quiz_start_time] = qs_time
    session[:exam_id] = exam_id
  end

  def quiz_session_destroy
    session.delete :quiz_session_id
    session.delete :quiz_start_time
    session.delete :exam_id
  end
end
