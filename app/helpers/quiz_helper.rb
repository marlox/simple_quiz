module QuizHelper
  def has_reach_max_time? qs
    @startTime = qs.created_at.to_s.to_time
    @maxTime = qs.quiz.max_time
    @endTime = (@startTime + @maxTime.minute)
    @currentTime = Quiz.now.to_s.to_time
    # @timeDiff = TimeDifference.between(@currentTime, @endTime)

    (!session[:quiz_session_id].blank? && (@endTime - @currentTime) < 1)
  end
end
