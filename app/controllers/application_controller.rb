class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_action :set_raven_context, :configure_permitted_parameters, if: :devise_controller?


  private

  def set_raven_context
    Raven.user_context(account: current_user.email) if current_user# or anything else in session
    Raven.extra_context(params: params.to_hash, url: request.url)
  end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :email, :address, :phone_number])
      devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :email, :address, :phone_number])
    end

  def after_sign_in_path_for(resource)
    # check for the class of the object to determine what type it is

    #raise resource.model_name.name.inspect
    case resource.model_name.name
    when "AdminUser"
      admin_dashboard_path
    when "User"
      root_path
    end
  end

  # def set_raven_context
  #   if current_user
  #     Raven.user_context(account: current_user.email)
  #   end

  #   Raven.extra_context(params: params.to_unsafe_hash, url: request.url)
  # end
end
