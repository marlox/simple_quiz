include QuizHelper

class QuizzesController < ApplicationController

  before_action :setup_or_gather_quiz_session, :generate_timer, except: :result

  def question
   @quiz = Quiz.find(params[:quiz_id])
   @question = @qs.find_unanswered_question
  end

  def answer
    qa = QuestionAnswer.create(param_permits)
    qa.quiz_session = @qs
    qa.check_answer(qa)
    qa.save!

    # @nxt_quiz_id = get_next_quiz?

    # if @qs.is_done? && @nxt_quiz_id
    #   redirect_to quiz_question_path(params[:quiz_id])

    # elsif @qs.is_done?
    #   redirect_to quiz_result_path(params[:quiz_id])
    #   session.delete(:quiz_session_id)
    #   session.delete(:quiz_start_time)
    if @qs.is_done?
      redirect_to root_path
    else
      redirect_to quiz_question_path(params[:quiz_id])
    end
  end

  def result

  end

private
  def param_permits
    params.require(:question_answer).permit(:answer, :question_id)
  end

  def setup_or_gather_quiz_session
    if session[:quiz_session_id].blank?
      redirect_to root_path
    else
      @qs = QuizSession.find(session[:quiz_session_id])
    end
  end

	def generate_timer
		@maxTime = @qs.quiz.max_time
		#@hour = @minute = @second = @maxTime
		return true if @maxTime < 1

		if has_reach_max_time?(@qs)
			#reset_session
      # session.delete(:quiz_session_id)
      # session.delete(:quiz_start_time)
			# redirect_to quiz_result_path(params[:quiz_id]), alert: "Time's Up!" and return
      redirect_to root_path
		end

    @total_seconds = (Time.now - @qs.created_at)
    @max_seconds = @maxTime.minute.to_i

    @hours = (@total_seconds / (60 * 60))
    @minutes = ((@total_seconds / 60) % 60)
    @seconds = (@total_seconds % 60)

    @hours, @minutes, @seconds = [@hours, @minutes, @seconds].map{|t| t.to_i.to_s.rjust(2, "0") }

	end

end
