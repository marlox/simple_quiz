include HomeHelper
include QuizHelper

class HomeController < ApplicationController
	before_action :authenticate_user!, only: [:index]

	def index
		@exams = Exam.all #@TO-DO: all should be replaced with .find_by(office_id: curent_user.office_id) upon office implementation
		@qs_id = session[:quiz_session_id]
		@exam_started = !@qs_id.blank? && !session[:exam_id].blank?
		@exam = @exams.first
		@quiz = @exam.quizzes.first
		if @exam_started
			@quizSession = QuizSession.find @qs_id
			if @quizSession.is_done? || has_reach_max_time?(@quizSession)
				@exam = @exams.find session[:exam_id]
				@quiz = @exam.get_next_quiz @quizSession.quiz
				quiz_session_destroy
			else
				redirect_to quiz_question_path(params[:quiz_id])
			end
			# result page redirection when quiz is blank
			redirect_to quiz_result_path(@quizSession.quiz.id) if @quiz.blank?
		elsif @exams.count > 1 && !@exam_started #@TO-DO: should be tested for multiple exams (change if needed)
			@quiz = nil
		end
		session.delete(:quiz_session_id) unless session[:quiz_session_id].blank?
	end

	def quiz_session
		@quizSession = QuizSession.create(quiz_session_params)
		@quizSession.user = current_user
		@quizSession.first_name = @quizSession.last_name = @quizSession.email = ""
		if @quizSession.save
			quiz_session_create @quizSession.id, @quizSession.created_at, params[:exam_id]
			redirect_to quiz_question_path(@quizSession.quiz_id)
		else
			@errors = @quizSession.errors if @quizSession.errors.any?
			redirect_to root_path, alert: @errors.to_a.join(", ").to_s
		end
	end

private
	def quiz_session_params
		params.permit(:first_name, :last_name, :email, :address, :phone_number, :quiz_id)
	end
end