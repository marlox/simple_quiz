$(document).on "turbolinks:load ready", ->

	max_seconds = $('#countdown-timer').data('max-seconds')

	refreshTime = (hours, minutes, seconds) ->


		setInterval((->
			seconds++

			if seconds == 60
				seconds = 0
				minutes++
				if minutes == 60
					minutes = 0
					hours++

			newTime = hours + ':' + minutes + ':' + seconds

			$("#timer-second").text(lpad(seconds))
			$("#timer-minute").text(lpad(minutes))
			$("#timer-hour").text(lpad(hours))

			total_seconds = (seconds + (minutes * 60) + ((hours * 60) * 60))
			total_seconds++

			progress total_seconds, $('.progressBar')

			if total_seconds >= max_seconds
				location.reload();
		), 1000)

	progress = (total_seconds, $element) ->

	  progressBarWidth = total_seconds * $element.width()/max_seconds;
	  #console.log total_seconds, max_seconds, progressBarWidth

	  $element.find('div').animate({ width: progressBarWidth }, 500).html #total_seconds + ' sec'
	  if total_seconds >= max_seconds
	    setTimeout (->
	      progress total_seconds + 1, max_seconds, $element
	      return
	    ), 1000
	  return

	lpad = (i) ->
		if parseInt(i) < 10
			i = '0' + i
		i
	# JS for Quiz Description Selector
	$('#quiz_id').change ->
		selectedQuiz = $(this).val()
		$('[id|=quiz-info]').hide()
		$('#quiz-info-'+ selectedQuiz).show()
		return

	if ($('#quiz_id').length > 0)
		$('#quiz_id').find("option:eq(0)").prop('selected', 'selected').change()

	$('#exam_id').change ->
		selectedQuiz = $(this).val()
		$('[id|=quiz-info]').hide()
		$('#quiz-info-'+ selectedQuiz).show()
		return

	if ($('#exam_id').length > 0)
		$('#exam_id').find("option:eq(0)").prop('selected', 'selected').change()

	if ($('#countdown-timer').length > 0)
		seconds = parseInt($("#timer-second").text())
		minutes = parseInt($("#timer-minute").text())
		hours = parseInt($("#timer-hour").text())
		refreshTime(hours, minutes, seconds)

	return
