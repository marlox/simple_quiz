ActiveAdmin.register QuizSession, as: 'Results' do
	actions :index, :show, :destroy
	preserve_default_filters!
	remove_filter :question_answers, :created_at, :updated_at
	
	breadcrumb { ['admin', 'quizzes', 'results'] }

	index do
		column :applicant_name
		column :user_email
		column :user_phone_number
		column 'Quiz', :quiz_name
		column(:score) { |c| c.score(c.id) }
		column(:percentage) { |c| link_to number_to_percentage(c.percentage(c.id), format: "%n%", precision: 0), admin_result_path(c), title: 'Show Details' }
	end
	
	show title: "Applicant's Result Information" do |qs|
		tabs do
			tab 'Summary' do
				panel 'Basic Result Info' do
					div { strong "Name: <u>#{qs.applicant_name}</u>".html_safe }
					div { strong "Email: <u>#{qs.user.try(:email)}</u>".html_safe }
					div { strong "Address: <u>#{qs.user.try(address)}</u>".html_safe }
					# div { strong "Phone Number: <u>#{qs.user.try(phone_number)}</u>".html_safe }
					hr
					div { strong "Quiz: <u>#{qs.quiz_name}</u>".html_safe }
					div { strong "Score: <u>#{qs.score(qs.id)}</u>".html_safe }
					div { strong "Percentage: <u>#{number_to_percentage(qs.percentage(qs.id), format: "%n%", precision: 0)}</u>".html_safe }
				end
			end

			tab 'Details' do
				panel 'Detailed Results' do
					div { h3 "#{qs.applicant_name} got <u>#{number_to_percentage(qs.percentage(qs.id), format: "%n%", precision: 0)}</u> after taking the <em>#{qs.quiz_name}</em> with the following results:".html_safe }
					qs.question_answers.each_with_index do |ans, ctr|
						div class: "panel panel-body" do
							status_tag (ans.is_good ? "C" : "Inc") +"orrect", (ans.is_good ? :ok : :error)
							br 
							div { strong "Question #{ctr + 1}: #{ans.question_label(ans.question_id)}" }
							div { strong "<u>Answered: #{ans.answered_value(ans.question_id, ans.answer)}</u>".html_safe }
							div { strong "Correct Answer: #{ans.correct_answer(ans.question_id)}" }
						end
					end
				end
			end
		end
		active_admin_comments
	end
end