ActiveAdmin.register Question do
	preserve_default_filters!
	remove_filter :question_answers, :updated_at, :option5, :option6, :image_content_type, :image_file_size
	permit_params :label, :option1, :option2, :option3, :option4, :option5, :option6, :good_answer, :quiz_id, :image
	
	breadcrumb { ['admin', 'quizzes', 'questions'] }

	index do
		column 'Question', :label
		column 'Answer', :good_answer
		column 'Image' do |q|
			image_tag q.image.try(:url, :thumb) if !q.image.try(:url, :thumb).include? 'missing'
			#image_tag q.image.url(:thumb) if !q.image.url(:thumb).include? 'missing'
		end
		actions
	end
	
	form html: { :enctype => "multipart/form-data" } do |f|
		f.inputs do
			f.input :quiz
			f.input :label, label: 'Question'
			f.input :option1, required: true
			f.input :option2, required: true
			f.input :option3
			f.input :option4
			f.input :option5
			f.input :option6
			f.input :good_answer, label: 'Answer', as: :select, collection: {'Option1'=>1,'Option2'=>2,'Option3'=>3,'Option4'=>4,'Option5'=>5,'Option6'=>6}
			f.input :image, as: :file
		end
		f.actions
	end

	show title: 'Question Details' do
		attributes_table do
			row('Question') { |q| q.label }
			row :option1
			row :option2
			row :option3
			row :option4
			row :option5
			row :option6
			row('Answer') { |q| "OPTION#{q.good_answer}" }
			row('Image') { |q| image_tag q.image.try(:url) }
			row :created_at
			row :updated_at
		end
		active_admin_comments
	end

	action_item :new, except: [:index, :new] do
		link_to 'New Question', new_admin_question_path
	end
end