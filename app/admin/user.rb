ActiveAdmin.register User do
  permit_params :first_name, :last_name, :email, :address, :phone_number, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :email
    column :phone_number
    column :address
    column :created_at
    actions
  end

  filter :first_name
  filter :last_name
  filter :email
  filter :phone_number
  filter :address
  filter :created_at

  form do |f|
    f.inputs "Applicant Details" do
    	f.input :first_name
    	f.input :last_name
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
