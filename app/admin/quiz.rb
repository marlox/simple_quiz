ActiveAdmin.register Quiz do
	preserve_default_filters!
	remove_filter :quiz_sessions, :questions
	permit_params :name, :instruction, :max_time, :max_attempts, :rank, :exam_id
	
	breadcrumb { ['admin', 'quizzes'] }
	
	form do |f|
		f.inputs do
			f.input :exam
			f.input :name, label: 'Quiz Name'
			f.input :instruction
			f.input :max_time, label: 'Maximum Time (min)'
			f.input :max_attempts, label: 'Maximum Attempts'
			f.input :rank
		end
		f.actions
	end
	action_item :new, except: [:index, :new] do
		link_to 'New Quiz', new_admin_quiz_path
	end
end