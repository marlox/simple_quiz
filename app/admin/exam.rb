ActiveAdmin.register Exam do
	preserve_default_filters!
	remove_filter :quizzes
	permit_params :name, :description, :rank
	
	breadcrumb { ['admin', 'exams'] }
	
	form do |f|
		f.inputs do
			f.input :name, label: 'Exam Name'
			f.input :description
			f.input :rank
		end
		f.actions
	end
end