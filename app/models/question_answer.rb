class QuestionAnswer < ApplicationRecord
	belongs_to :quiz_session
	belongs_to :question

	validates :quiz_session, presence: true
	validates :question, presence: true

	def check_answer questionAnswer
		self.is_good = (Question.find(questionAnswer.question.id).good_answer == questionAnswer.answer)
	end

	def question_label questionID
		Question.find(questionID).label
	end

	def answered_value questionID, answered
		Question.where(id: questionID).pluck("option#{answered}").first
	end

	def correct_answer questionID
		@correctAnswer = Question.where(id: questionID).pluck("good_answer").first
		Question.where(id: questionID).pluck("option#{@correctAnswer}").first
	end
end