class Exam < ApplicationRecord
	has_many :quizzes

	validates :name, presence: true, uniqueness: true
  validates_uniqueness_of :rank

  default_scope -> { order(rank: :asc) }

  def get_next_quiz current_quiz
    quizzes.where("rank = ?", current_quiz.rank + 1).first
  end
end