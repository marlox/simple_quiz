class QuizSession < ApplicationRecord
	belongs_to :quiz
  belongs_to :user

  has_many :question_answers, dependent: :destroy

  validates :user, presence: true
  validates :quiz, presence: true
	validate :have_reached_attempt_limit, on: :create

  def find_unanswered_question
    already_answered_questions = self.question_answers.pluck(:question_id)
    self.quiz.questions.where.not(id: already_answered_questions).first
  end

  def is_done?
    find_unanswered_question.nil?
  end

  def quiz_name
  	self.quiz.name
  end

  def applicant_name
  	"#{self.user.try(:first_name)} #{self.user.try(:last_name)}"
  end

  def user_email
    self.user.email
  end

  def user_phone_number
    self.user.phone_number
  end

	def get_correct_answers quiz_session_id
		self.question_answers.where(quiz_session_id: quiz_session_id, is_good: true).count
	end

	def get_number_of_questions
		self.quiz.questions.all.count
	end

	def score quiz_session_id
		"#{self.get_correct_answers(quiz_session_id)} / #{self.get_number_of_questions}"
	end

	def percentage quiz_session_id
		self.get_correct_answers(quiz_session_id).to_f.send(:/, self.get_number_of_questions).send(:*, 100)
	end

	def have_reached_attempt_limit
		@max_attempts = Quiz.find(self.quiz_id).max_attempts
		return true if @max_attempts < 1
		@attempts = QuizSession.where("(email = ? OR first_name ||' '|| last_name = ?) AND quiz_id = ?", self.email, self.applicant_name, self.quiz_id).count
		if @attempts >= @max_attempts
			errors.add(:maximum_attempts, "have been reached by #{self.email}. Kindly wait for feedback regarding the assessment of your job application from Hiring Personnel. Thank you!")
		end
	end
end