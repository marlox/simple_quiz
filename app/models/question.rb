class Question < ApplicationRecord
	has_many :question_answers, dependent: :destroy
	belongs_to :quiz

	validates :label, presence: true, allow_blank: false
	validates :option1, presence: true, allow_blank: false
	validates :option2, presence: true, allow_blank: false
	validates :good_answer, presence: true, allow_blank: false
	validates :quiz, presence: true

	#has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>", default: "800x800>" }, default_url: "/images/:style/missing.png"
  # has_attached_file :image,
  #                   :styles => { :big => "800x800>", :medium => "300x250>", :thumb => "100x100>" },
  #                   :storage => :fog,
  #                   :fog_credentials => "#{Rails.root}/config/gce.yml",
  #                   :fog_directory => "ingedata-job",
  #                   :path => ":rails_root/public/users/:id/:style/:basename.:extension",
  #                   :url => "/users/:id/:style/:basename.:extension",
  #                   :default_url => "/images/:style/missing.png"

  has_attached_file :image,
                    :styles => { :big => "800x800>", :medium => "300x250>", :thumb => "100x100>" },
                    :default_url => "/images/:style/missing.png",
                    :size => { :less_than => 1.megabyte }

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

end