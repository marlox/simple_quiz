class Quiz < ApplicationRecord
	has_many :quiz_sessions
	has_many :questions
  belongs_to :exam

	validates :name, presence: true
  validates_uniqueness_of :rank, scope: :exam_id

  default_scope -> { order(rank: :asc) }
	scope :now, -> { pluck("now()") }
end