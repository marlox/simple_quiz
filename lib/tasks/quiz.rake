desc "Assign quiz to user"
namespace :quiz do
  task :fix => :environment do

    quiz_sesssions = QuizSession.all
    quiz_sesssions.each do |q|

      user = User.where(email: q.email).first_or_create do |u|
        u.first_name = q.first_name
        u.last_name = q.last_name
        u.address = q.address
        u.phone_number = q.phone_number
        u.password = SecureRandom.hex(4)
      end

      puts "Assigning quiz session ##{q.id} to #{user.email}"

      q.user = user
      q.save!
    end

  end
end