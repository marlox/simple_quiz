class AddRankToQuizzes < ActiveRecord::Migration[5.0]
  def change
    add_column :quizzes, :rank, :integer, unique: true
  end
end
