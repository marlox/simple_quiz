class AddOptionsToQuestions < ActiveRecord::Migration[5.0]
  def change
    add_column :questions, :option5, :string, after: 'option4'
    add_column :questions, :option6, :string, after: 'option5'
  end
end
