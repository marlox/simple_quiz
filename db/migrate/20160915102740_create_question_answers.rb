class CreateQuestionAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :question_answers do |t|
      t.integer :answer
      t.boolean :is_good
      t.references :quiz_session, foreign_key: true
      t.references :question, foreign_key: true
      t.timestamps null: false
    end
  end
end
