class CreateQuizSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :quiz_sessions do |t|
      t.string :first_name, index: true, null: false
      t.string :last_name, index: true, null: false
      t.string :email, index: true, null: false
      t.string :address
      t.string :phone_number
      t.references :quiz, foreign_key: true
      t.timestamps null: false
    end
  end
end
