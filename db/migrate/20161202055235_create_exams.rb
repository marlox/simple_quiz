class CreateExams < ActiveRecord::Migration[5.0]
  def change
    create_table :exams do |t|
      t.string :name, index: true, null:false
      t.text :description
      t.integer :rank, unique: true
    end

    change_table :quizzes do |t|
      t.references :exam
    end
  end
end
