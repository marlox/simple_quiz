class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.string :label, index: true, null: false
      t.string :option1
      t.string :option2
      t.string :option3
      t.string :option4
      t.integer :good_answer, index: true, null: false
      t.references :quiz
      t.timestamps null: false
    end
  end
end
