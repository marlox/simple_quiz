class AddUserIdToQuizzes < ActiveRecord::Migration[5.0]
  def change
    add_column :quiz_sessions, :user_id, :integer
  end
end
