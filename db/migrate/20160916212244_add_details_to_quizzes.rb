class AddDetailsToQuizzes < ActiveRecord::Migration[5.0]
  def change
    add_column :quizzes, :instruction, :text
    add_column :quizzes, :max_time, :integer, default: 1
    add_column :quizzes, :max_attempts, :integer, default: 1
  end
end
