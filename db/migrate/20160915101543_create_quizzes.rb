class CreateQuizzes < ActiveRecord::Migration[5.0]
  def change
    create_table :quizzes do |t|
      t.string :name, index: true, null: false
      t.timestamps null: false
    end
  end
end
