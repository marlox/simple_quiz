ActiveRecord::Base.transaction do
  @email = "admin@ingedata.net"
  AdminUser.create!(email: @email, password: "Access123") unless AdminUser.find_by(email: @email)

  @exam = "Exam 1"
  @ex = Exam.find_by name: @exam
  @ex = Exam.create!(name: @exam, rank: 1) unless @ex

  Dir["data/quizzes/**/quiz.yaml"].each do |file|

    yaml = YAML::load(File.read(file)).with_indifferent_access

    @quiz = Quiz.find_by yaml.select{|k,v| ["name"].include?(k.to_s) }

    if @quiz.blank?

      @quiz = Quiz.create! yaml.select{|k,v| ["name", "instruction", "max_time", "max_attempts"].include?(k.to_s) }

      @quiz.update_attributes!(exam_id: @ex.id) unless @ex.blank?

      yaml[:questions].each do |question|
        image = question[:image]

        #  if image
        #    image = File.new(File.join(File.dirname(file), image))
        #  end

        @quiz.questions.create! question.merge(image: image)
      end
    end
  end
end